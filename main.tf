# provider "aws" {
#   region = "us-east-1"  # Adjust the region as needed
# }

module "waf_alb_integration" {
  source           = "./waf-alb-integration-module"
  web_acl_name     = "my-web-acl"
  metric_name      = "MyWAFMetric"
  default_action_type = "BLOCK"

  rules = [
    {
      name        = "Rule1"
      priority    = 1
      action_type = "BLOCK"
      statements  = [
        {
          rule_group_name = "AWSManagedRulesCommonRuleSet"
        },
      ]
    },
    # Add more rules as needed
  ]

  alb_name                     = "my-alb"
  alb_internal                 = false
  alb_type                     = "application"
  alb_enable_deletion_protection = true

  alb_listener_port     = 80
  alb_listener_protocol = "HTTP"

  alb_target_group_name    = "my-target-group"
  alb_target_group_port    = 80
  alb_target_group_protocol = "HTTP"

  vpc_id = "vpc-0a0b257b1828381f3"  # Replace with your VPC ID
}

# output "web_acl_id" {
#   value = module.waf_alb_integration.web_acl_id
# }
