resource "aws_waf_web_acl" "web_acl" {
  name     = var.web_acl_name
  metric_name  = var.metric_name
    default_action {
    type = var.default_action_type
  }
   dynamic "rule" {
    for_each = var.rules

    content {
      name        = rule.value["name"]
      priority    = rule.value["priority"]
      action {
        type = rule.value["action_type"]
      }

      dynamic "statement" {
        for_each = rule.value["statements"]

        content {
          rule_group_reference_statement {
            name = statement.value["rule_group_name"]
          }
        }
      }
    }
  }
}

resource "aws_lb" "alb" {
  name               = var.alb_name
  internal           = var.alb_internal
  load_balancer_type = var.alb_type

  enable_deletion_protection = var.alb_enable_deletion_protection
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = var.alb_listener_port
  protocol          = var.alb_listener_protocol

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}


resource "aws_lb_target_group" "alb_target_group" {
  name     = var.alb_target_group_name
  port     = var.alb_target_group_port
  protocol = var.alb_target_group_protocol
  vpc_id   = var.vpc_id
}

resource "aws_wafregional_web_acl_association" "alb_waf_association" {
  resource_arn = aws_lb.alb.arn
  web_acl_id   = aws_waf_web_acl.web_acl.id
}



