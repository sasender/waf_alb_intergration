variable "web_acl_name" {
  description = "Name for the WAF Web ACL"
  type        = string
}

variable "metric_name" {
  description = "Name for the CloudWatch metric"
  type        = string
}

variable "default_action_type" {
  description = "Type of the default action"
  type        = string
}

variable "rules" {
  description = "List of WAF rules"
  default = null
  type        = list(object({
    name        = string
    priority    = number
    action_type = string
    statements  = list(object({
      rule_group_name = string
    }))
  }))
}

variable "alb_name" {
  description = "Name for the ALB"
  type        = string
}

variable "alb_internal" {
  description = "Whether the ALB is internal or internet-facing"
  type        = bool
}

variable "alb_type" {
  description = "Type of load balancer (application, network, etc.)"
  type        = string
}

variable "alb_enable_deletion_protection" {
  description = "Enable deletion protection for the ALB"
  type        = bool
}

variable "alb_listener_port" {
  description = "Port for the ALB listener"
  type        = number
}

variable "alb_listener_protocol" {
  description = "Protocol for the ALB listener"
  type        = string
}

variable "alb_target_group_name" {
  description = "Name for the ALB target group"
  type        = string
}

variable "alb_target_group_port" {
  description = "Port for the ALB target group"
  type        = number
}

variable "alb_target_group_protocol" {
  description = "Protocol for the ALB target group"
  type        = string
}

variable "vpc_id" {
  description = "VPC ID"
  type        = string
}

